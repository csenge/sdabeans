package edu.sda.BeanExample.model;

public class Message {

    private String message;

    public String getMessage() {
        System.out.println("Your message is: " + message);
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
