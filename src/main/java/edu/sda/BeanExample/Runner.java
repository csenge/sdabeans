package edu.sda.BeanExample;

import edu.sda.BeanExample.model.Message;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Runner {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Message message = (Message) context.getBean("defaultMessage");
        message.getMessage();
        message = (Message) context.getBean("sdaMessage");
        message.getMessage();
    }
}
